%letra a) Um predicado que insira um elemento no final da lista
%
insereFinal([], Item, [Item]).
insereFinal([X|Y], Item, [X|Resultado]):- insereFinal(Y,Item,Resultado).
insereFinal(X,Y, Resultado):- concatena([X],[],Temp), concatena(Temp,[Y], Resultado).

% Descricao: Reccebe uma lista e um elemento unitario para ser inserido
% na lista. Para isso utiliza uma funcao extra de concatenacao.

% Uso: insereFinal(Lista, Item, Resultado).
%  Lista: Uma lista qualquer. Pode conter qualquer tipo de item. Deve
%  ser do formato [X]
%
%  Item: Item a ser inserido
%
%  Resultado: Lista concatenada com o item a ser inserido no final
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

% letra b) Um predicado que insira um elemento em uma determinada
% posi��o da lista
%
inserePosicao(Lista, 1, Item, [Item | Lista]).
inserePosicao([X|Y],Pos, Item, [X|Resultado]):-  M is Pos -1, inserePosicao(Y, M, Item, Resultado).

% Descricao: Insere um item qualquer dada a posicao que o usuario der. E
% assumido que o numero dado pelo usuario e sempre menor ou igual ao
% tamanho da lista.

% Uso: inserePosicao(Lista, Pos, Item, Resultado).
%  Lista: Uma lista qualquer. Pode conter qualquer tipo de item. Deve
%  ser do formato [X]
%
%  Posicao: Numero menor ou igual ao tamanho da lista
%
%  Item: Item a ser inserido
%
%  Resultado: Lista com o item inserido
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

%letra c) Um predicado que remova um elemento em uma determinada posi��o da lista.
%
removePosicao([_|Y],1,Y).
removePosicao([X|Y],Pos,[X|Resultado]):- M is Pos -1, removePosicao(Y,M,Resultado).

% Descricao: Remove um item qualquer dada a posicao que o usuario der. E
% assumido que o numero dado pelo usuario e sempre menor ou igual ao
% tamanho da lista.

% Uso: removePosicao(Lista, Pos, Resultado).
%  Lista: Uma lista qualquer. Pode conter qualquer tipo de item. Deve
%  ser do formato [X]
%
%  Posicao: Numero menor ou igual ao tamanho da lista
%
%  Resultado: Lista com o item removido
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


%letra d) Um predicado que forne�a a intersec��o de duas listas.
%

intersecao(_,[],[]).
intersecao(L1,[X|Y],Resultado):- verifica(L1,[X],Bit), intersecao(L1,Y,ResultadoTemp),((Bit is 1 ,concatena([X],ResultadoTemp,Resultado)); (Bit is 0, concatena([],ResultadoTemp,Resultado))).

% Descricao: Verifica e retorna todos os itens que pertencem as duas
% listas (L1 e L2)

% Uso: intersecao(L1, L2, Resultado)
%  L1: Lista 1
%
%  l2: lista 2
%
%  Resultado: Lista contendo todos os itens que pertencem as duas listas
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


%letra e) Um predicado que identifique se um conjunto de elementos est� contido em uma lista (os elementos do conjunto podem estar em qualquer ordem na lista).
%
criaLista(_,[],1).
criaLista(ListaBase, [X|Y], BitResultado):- percorreLista(ListaBase,X,BitVerificacao), ((BitVerificacao is 0, BitResultado is 0); criaLista(ListaBase, Y, BitResultado)).
verifica(ListaBase, ListaElementos,BitVerificacao):- criaLista(ListaBase,ListaElementos, BitVerificacao).

% Descricao: Verifica se um conjunto de itens pertence a lista. A
% verificacao nao leva em consideracao a ordem.

% Uso: verifica(L1, L2, Resultado).
%
%  L1: Lista base. Essa lista e onde os itens serao verificados
%
%  L2: Lista a ser verificada
%
%  Resultado: Retorna 1 se TODOS os itens de L2 estao em L1 e 0, caso
% contrario.
%
% Exemplos de uso:
%  verifica([1,2,3,3,2,1,0],[1,1,1],B).   => B = 1
%  verifica([1,2,3,3,2,1,0],[],B).        => B = 1
%  verifica([1,2,3,3,2,1,0],[0,2,4,5],B). => B = 0
%  verifica([1,2,3,3,2,1,0],[5],B).       => B = 0
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


% letra f)Um predicado que retorne o maior valor contido em uma lista
% num�rica.
%

verificaMaior([X|Y], Maior):- verificaMaiorBase(Y,X,Maior).
verificaMaiorBase([],X,X).
verificaMaiorBase([X|Y], MaiorTemp, MaiorFinal):- ((X>MaiorTemp, verificaMaiorBase(Y,X,MaiorFinal)); (X<MaiorTemp, verificaMaiorBase(Y,MaiorTemp,MaiorFinal))).

% Descricao: Verifica o maior valor numerico contido em uma lista. E
% assumido que a lista contem APENAS valores numericos

% Uso: verificaMaior(ListaNumerica,Resultado).
%  ListaNumerica: Lista com valores numericos
%
%  Resultado: Resultado com o maior numero da lsita
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


%letra g) Um predicado denominado ordena(X, Y) onde Y � uma vers�o ordenada da lista X.

removeBaseItem([X|Y],Item,Resultado):- ((X is Item),(concatena([],Y,Resultado)));(dif(Item,X),removeBaseItem(Y,Item,ResultadoTemp),insereFinal(ResultadoTemp,X,Resultado)).

ordena([],[]).
ordena(X,Y):- verificaMaior(X,Maior), removeBaseItem(X,Maior,T),ordena(T,ListaSemItem), insereFinal(ListaSemItem,Maior,Y).

% Descriacao: Ordena uma lista numerica

% ordena(L1, L2).
%  L1: Lista a ser ordenada
%
%  L2: Lista ordenada.
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

%letra h) Um predicado para obter a soma dos N primeiros n�meros naturais.
soma(0,0).
soma(Cont,Resultado):- M is Cont-1,soma(M,N), Resultado is N+Cont.

% Descricao: somatorio dos N numeros naturais

% Uso: soma(N, Resultado).
%  N: valor maximo da soma
%
%  Resultado: Resultado do somatorio
% @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

% Extra: Concatena duas listas. Utilizada como auxiliar em outros
% predicados. Metodo retirado do livro
concatena([],L,L).
concatena([X|L1],L2,[X|L3]):-concatena(L1,L2,L3).

%letra i) Escreva um predicado denominado pal�ndromo(X) que � verdadeiro se X � uma lista cujos elementos invertidos produzem a mesma ordem original.
%
inverte([],[]).
inverte([X|Y], Resultado):- inverte(Y,ResultadoTemp), concatena(ResultadoTemp,[X],Resultado).

palindromo(X):-inverte(X,Resultado), Resultado=X.

% Descricao: Verifica se uma lista e palindromo, ou seja, a lista
% cujos elementos estao em ordem invertida produzem o mesmo resultado.
% Retorna true se for verdadeiro e false caso nao seja
%
% Uso: palindromo(Lista).
%  Lista: Lista com os itens a serem verificados


%letra j) Escreva um predicado denominado acomoda/2 cujo primeiro argumento � uma lista permitindo listas como elementos (ex. [a, [a, [b, c]], b, [c, d]]) e cujo segundo argumento � outra lista com todos os elementos da primeira acomodados em uma �nica lista (ex. [a, a, b, c, b, c, d].
%

testeLista([_|_]).
acomoda2([],[]).
acomoda2([X|Y],Resultado):- acomoda2(Y,ResultadoTemp1), ((testeLista(X),acomoda2(X,ResultadoTemp2),concatena(ResultadoTemp1,ResultadoTemp2,Resultado)); (not(testeLista(X)),concatena([X],ResultadoTemp1,Resultado))).

% Descricao: Retorna a conversao de uma lista contendo multiplas listas
% em uma so lista. Observacao, caso uma lista contenha multiplas listas
% vazias como por exemplo [[[[[]]]]], item vazia sera considerada um
% item, portanto o resultado A sera A =[[]].

% Uso acomoda2(Lista, Resultado).
%  Lista: Lista composta por multiplas listas. Exemplo [a, [a, [b, c]], b, [c, d]]
%
%  Resultado: Resultado da lista convertida em uma unica lista. Ex [a, a, b, c, b, c, d]

percorreLista([],_,0).
percorreLista([X|Y],Item,Resultado):-(X is Item, Resultado is 1); percorreLista(Y, Item, Resultado).


% //////////////////////////////////////////////////////////////////////////////////////////
% PARTE 2 DO TRBALHO

%Premissas
canario(piupiu).
peixe(nemo).
tubarao(tutu).
vaca(mimosa).
morcego(vamp).
avestruz(xica).
salmao(alfred).


pele(X) :- animal(X).
%Todo animal possui pele.

animal(X) :- tipoPeixe(X).
animal(X) :- passaro(X).
animal(X) :- mamifero(X).
% passaros, mamiferos e peixes sao animais

nadar(X) :- tipoPeixe(X).
nadadeira(X) :-tipoPeixe(X).
%Se e peixe entao possui nadadeira e consequentemente nada

asas(X) :- passaro(X); morcego(X).
voar(X) :- (passaro(X),not(avestruz(X)));morcego(X).
% Se e um passaro entao voa. Exceto o avestruz que nao voa. Alem disso o
% mamifero morcego tambem voa

porOvos(X) :- (tipoPeixe(X),not(tubarao(X))),not(mamifero(X));passaro(X).
%Se nao e mamifero e nao e tubarao, entao bota ovos

tipoPeixe(X) :- tubarao(X).
tipoPeixe(X) :- salmao(X).
tipoPeixe(X) :- peixe(X).
% Tubarao, salmao e peixes sao tipos de peixe.

passaroAmarelo(X) :- canario(X).
%Canarios sao passaros amarelos.

passaro(X) :- avestruz(X); canario(X).
% Avestruz e canarios sao tipos de passaros

andar(X) :- avestruz(X);( mamifero(X), not(morcego(X))),not(tipoPeixe(X)).
%Mamiferos (exceto morcego) e avestruz sao tipos de animais que andam

mamifero(X) :- vaca(X); morcego(X).
%Vacas e morcegos sao mamiferos

darLeite(X) :- vaca(X).
%Vaca da leite que e utilizado pelo ser humano

comida(X) :- vaca(X); salmao(X).
%Vacas e salmao sao animais comestiveis

andamNaoPoeOvos(X):- andar(X), not(porOvos(X)).
%Animais que andam mas que nao botam ovos
